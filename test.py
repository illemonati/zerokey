#! /usr/bin/env python3

import keys
import time


def demo():
    with open('test_message.txt') as message:
        for line in message.readlines():
            keys.write_string(line)

if __name__ == '__main__':
    # time.sleep(3)
    demo()