import re

r = re.compile(r"((0x[\d|a-z]+, )+)\s+\/\*\s(.+)\s+\*\/")

outfile = open('report_desc_py.txt', 'w')
outfile.write('[\n')

with open('default_report_desc.txt', 'r') as file:
    for line in file.readlines():
        ll = line.strip()
        h, _, d = re.findall(r, ll)[0]
        code = h.rstrip()
        description = d.rstrip()
        output = f"{code} # {description}"
        print(output)
        outfile.write('    ' + output + '\n')

outfile.write(']\n')
outfile.close()