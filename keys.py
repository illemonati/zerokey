#! /usr/bin/env python3

## Requires 3.9+

import re
import time
import os

BLANK = bytearray([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
SE_CONFIG_FILE = "sel_keys.txt"
DV_CONFIG_FILE = "dv_keys.txt"

SE_CONFIG_FILE = os.path.join(os.path.dirname(__file__), SE_CONFIG_FILE)
DV_CONFIG_FILE = os.path.join(os.path.dirname(__file__), DV_CONFIG_FILE)


class Key:
    def __init__(self, location, value):
        self.value = value
        self.location = location

class DVKey(Key):
    def __init__(self, bit_position, location, value):
        super().__init__(location, value)
        self.bit_position = bit_position

    def __repr__(self):
        return f"<DVKey> [{self.bit_position}] ({self.location}) {self.value}"

class SEKey(Key):
    def __init__(self, report_code, location, need_shift, value):
        super().__init__(location, value)
        self.report_code = report_code
        self.need_shift = need_shift
    
    def __repr__(self):
        return f"<SEKey> [{self.report_code}] ({self.location}) {self.value}"

def load_dv_keys(file):
    line_regex = re.compile(r"(\d) (\w+)\s(\w+)")
    keys = dict()
    with open(file, 'r') as config:
        for line in config.readlines():
            bit_position_raw, location, value = re.findall(line_regex, line)[0]
            bit_position = int(bit_position_raw)

            if location == "LEFT":
                keys[value] = DVKey(bit_position, location, value)

            keys[("R" if location == "RIGHT" else "L") + value] = DVKey(bit_position, location, value)
    return keys


SE_ALTS = {
    "Spacebar": [" "],
    "Backspace": ["\b"],
    "Tab": ["\t"],
    "Return (ENTER)": ["\n"],
}

def load_se_keys(file):
    line_regex = re.compile(r"([\d|\w]{2})\s(\w+)\s(.+)\sSel")
    keys = dict()
    with open(file, 'r') as config:
        for line in config.readlines():
            try:
                report_code_raw, location, values_raw = re.findall(line_regex, line)[0]
                report_code = int(report_code_raw, 16)
                values = values_raw.split(' and ')
                for i, value in enumerate(values):
                    value_alternatives = [value]
                    if value in SE_ALTS:
                        value_alternatives = [value, *SE_ALTS[value]]
                    for alternative in value_alternatives:
                        if location == "Keyboard":
                            keys[alternative] = SEKey(report_code, location, (i > 0),alternative)
                        keys[("Pad" if location == "Keypad" else "Board") + alternative] = SEKey(report_code, location, (i > 0),alternative)
            except IndexError as e:
                pass

    return keys

def load_keys(dv_file, se_file):
    dv = load_dv_keys(dv_file)
    se = load_se_keys(se_file)
    return dv | se

KEYS = load_keys(DV_CONFIG_FILE, SE_CONFIG_FILE)


#ie. "SHIFT a" or "A"
def keystring_to_bytearray(keystring, autoshift=True):
    if keystring == ' ':
        return bytearray([0, 0, KEYS[' '].report_code, 0, 0, 0, 0, 0])
    modifer_keys = 0b00000000
    se_keys = [0, 0, 0 ,0 ,0 ,0]
    se_key_index = 0
    for keystr in keystring.split(' '):
        key = KEYS[keystr]
        if isinstance(key, DVKey):
            modifer_keys |= (0b00000001 << key.bit_position)
        elif isinstance(key, SEKey):
            if se_key_index > len(se_keys):
                pass
            else:
                se_keys[se_key_index] = key.report_code
                if autoshift and key.need_shift:
                    modifer_keys |= (0b00000001 << KEYS["SHIFT"].bit_position)
    
    return bytearray([modifer_keys] + [0] + se_keys)


def write_keys(keys: bytearray):
    with open('/dev/hidg0', 'rb+') as fd:
        fd.write(bytes(keys))

def write_string(string: str):
    for i, c in enumerate(string):
        try:
            write_keys(keystring_to_bytearray(c))
            if len(string)-1 > i and string[i+1] == c:
                write_keys(BLANK)
        except KeyError as e:
            print("error")
            pass
    write_keys(BLANK)

def write_stringln(string: str):
    write_string(string + '\n')

if __name__ == '__main__':
    # print(KEYS)
    time.sleep(3)
    # write A
    write_keys(keystring_to_bytearray("RSHIFT a"))
    write_keys(BLANK)
    # write A again
    write_keys(keystring_to_bytearray("A"))
    write_keys(BLANK)
    # write comma
    write_keys(keystring_to_bytearray(","))
    write_keys(BLANK)
    # write \n
    write_keys(keystring_to_bytearray("\n"))
    write_keys(BLANK)
    # # write a string
    write_stringln("A: Hello There, How are you today")
    write_stringln("B: I'm doing okay, thank you!")

    

