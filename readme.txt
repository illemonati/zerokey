
preconfig
--------------------
/boot/config.txt: 
dtoverlay=dwc2

/etc/modules:
dwc2
libcomposite


configure_gadget
--------------------
configfs: https://www.kernel.org/doc/Documentation/usb/gadget_configfs.txt
vendor/device : https://the-sz.com/products/usbid/index.php, https://devicehunt.com/view/type/usb
hid: https://www.kernel.org/doc/html/v5.9/usb/gadget_hid.html
keyboard: https://stackoverflow.com/questions/27075328/list-of-hex-keyboard-scan-codes-and-usb-hid-keyboard-documentation
codes: https://usb.org/sites/default/files/hut1_3_0.pdf chapter 10
modifiers: https://www.usb.org/sites/default/files/documents/hid1_11.pdf chapter 8.3


add to boot
--------------------
add configure_gadget (optionally and test)to /etc/rc.local
