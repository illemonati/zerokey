#!/usr/bin/env python3

import os
import struct


GADGET_DIR = '/sys/kernel/config/usb_gadget/gadget'

VENDOR_ID = '0x05ac' # Apple, Inc.
PRODUCT_ID = '0x0221' # Aluminum Keyboard (ISO)
DEVICE_BCD = '0x0100' # Device Version 1.0.0
USB_BCD = '0x0200' # USB 2.0.0

LANG_ID = '0x409' # ENU (English - United States)

SERIAL_NUMBER = "e0af0e0fa" # random lol
MANUFACTURER_NAME = "Zero Key"
PRODUCT_NAME = "Zero Key"

HID_FUNCTION_NAME = "hid.usb0"
HID_PROTOCOL = "1" # Keyboard
HID_SUBCLASS = "0" # No Subclass
HID_REPORT_LENGTH = "8" 
HID_REPORT_DESC_RAW = [
    0x05, 0x01, # USAGE_PAGE (Generic Desktop)
    0x09, 0x06, # USAGE (Keyboard)
    0xa1, 0x01, # COLLECTION (Application)
    0x05, 0x07, #   USAGE_PAGE (Keyboard)
    0x19, 0xe0, #   USAGE_MINIMUM (Keyboard LeftControl)
    0x29, 0xe7, #   USAGE_MAXIMUM (Keyboard Right GUI)
    0x15, 0x00, #   LOGICAL_MINIMUM (0)
    0x25, 0x01, #   LOGICAL_MAXIMUM (1)
    0x75, 0x01, #   REPORT_SIZE (1)
    0x95, 0x08, #   REPORT_COUNT (8)
    0x81, 0x02, #   INPUT (Data,Var,Abs)
    0x95, 0x01, #   REPORT_COUNT (1)
    0x75, 0x08, #   REPORT_SIZE (8)
    0x81, 0x03, #   INPUT (Cnst,Var,Abs)
    0x95, 0x05, #   REPORT_COUNT (5)
    0x75, 0x01, #   REPORT_SIZE (1)
    0x05, 0x08, #   USAGE_PAGE (LEDs)
    0x19, 0x01, #   USAGE_MINIMUM (Num Lock)
    0x29, 0x05, #   USAGE_MAXIMUM (Kana)
    0x91, 0x02, #   OUTPUT (Data,Var,Abs)
    0x95, 0x01, #   REPORT_COUNT (1)
    0x75, 0x03, #   REPORT_SIZE (3)
    0x91, 0x03, #   OUTPUT (Cnst,Var,Abs)
    0x95, 0x06, #   REPORT_COUNT (6)
    0x75, 0x08, #   REPORT_SIZE (8)
    0x15, 0x00, #   LOGICAL_MINIMUM (0)
    0x25, 0x65, #   LOGICAL_MAXIMUM (101)
    0x05, 0x07, #   USAGE_PAGE (Keyboard)
    0x19, 0x00, #   USAGE_MINIMUM (Reserved)
    0x29, 0x65, #   USAGE_MAXIMUM (Keyboard Application)
    0x81, 0x00, #   INPUT (Data,Ary,Abs)
    0xc0, # END_COLLECTION
]
HID_REPORT_DESC = bytearray(HID_REPORT_DESC_RAW)


def setup():
    write_gadget_file('idVendor', VENDOR_ID)
    write_gadget_file('idProduct', PRODUCT_ID)
    write_gadget_file('bcdDevice', DEVICE_BCD)
    write_gadget_file('bcdUSB', USB_BCD)

    write_device_strings('serialnumber', SERIAL_NUMBER)
    write_device_strings('manufacturer', MANUFACTURER_NAME)
    write_device_strings('product', PRODUCT_NAME)

    write_config_string(1, 'Config 1: HID Keyboard')
    write_config_attr(1, 'MaxPower', '250')

    write_function('protocol', HID_FUNCTION_NAME, HID_PROTOCOL)
    write_function('subclass', HID_FUNCTION_NAME, HID_SUBCLASS)
    write_function('report_length', HID_FUNCTION_NAME, HID_REPORT_LENGTH)
    write_function('report_desc', HID_FUNCTION_NAME, HID_REPORT_DESC)

    link_function_to_config(1, HID_FUNCTION_NAME)

    add_to_UDC()


def write_gadget_file(filename, content):
    filepath = os.path.join(GADGET_DIR, filename)
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    if type(content) == str:
        content = content.encode('utf-8')
    with open(filepath, 'wb') as file:
        file.write(bytes(content))

def write_device_strings(filename, content):
    write_gadget_file(os.path.join('strings', LANG_ID, filename), content)

def write_config_attr(config_num, attr, content):
    write_gadget_file(os.path.join('configs', f'c.{config_num}', attr), content)

def write_config_string(config_num, content):
    write_gadget_file(os.path.join('configs', f'c.{config_num}', 'strings', LANG_ID, 'configuration'), content)

def write_function(filename, function_name, content):
    write_gadget_file(os.path.join('functions', function_name, filename), content)

def link_function_to_config(config_num, function_name):
    os.symlink(os.path.join(GADGET_DIR, 'functions', function_name), os.path.join(GADGET_DIR, 'configs', f'c.{config_num}', function_name))
    
def add_to_UDC():
    udc_content = '\n'.join(os.listdir('/sys/class/udc'))
    write_gadget_file('UDC', udc_content)
    

    



if __name__ == '__main__':
    setup()
